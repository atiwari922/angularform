import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  data = {
    sections: [
      {
        section: 'example section',
        subSections: [
          {
            subSectionName: 'example subSection',
            formFields: [
              {
                formFieldName: 'example FieldName',
              },
            ],
          },
        ],
      },
    ],
  };

  myForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.myForm = this.fb.group({
      sections: this.fb.array([]),
    });

    this.setSections();
  }

  addNewSection() {
    let control = <FormArray>this.myForm.controls.sections;
    control.push(
      this.fb.group({
        section: [''],
        subSections: this.fb.array([]),
      })
    );
  }

  deleteSection(index) {
    let control = <FormArray>this.myForm.controls.sections;
    control.removeAt(index);
  }

  addNewSubSection(control) {
    control.push(
      this.fb.group({
        subSectionName: [''],
        formFields: this.fb.array([]),
      })
    );
  }

  deleteSubSection(control, index) {
    control.removeAt(index);
  }

  addNewFormField(control) {
    control.push(
      this.fb.group({
        formFieldName: [''],
      })
    );
  }

  deleteFormField(control, index){
    control.removeAt(index);
  }

  setSections() {
    let control = <FormArray>this.myForm.controls.sections;
    this.data.sections.forEach((x) => {
      control.push(
        this.fb.group({
          section: x.section,
          subSections: this.setSubSections(x),
        })
      );
    });
  }

  setSubSections(x) {
    let arr = new FormArray([]);
    x.subSections.forEach((y) => {
      arr.push(
        this.fb.group({
          subSectionName: y.subSectionName,
          formFields : this.setFormFields(y),
        })
      );
    });
    return arr;
  }

  setFormFields(x) {
    let arr = new FormArray([]);
    x.formFields.forEach((y) => {
      arr.push(
        this.fb.group({
          formFieldName: y.formFieldName,
        })
      );
    });
    return arr;
  }
}
